﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringCalculator.Tests
{
    [TestClass]
    public class StringCalculatorTests
    {
        private static StringCalculator BuildStringCalculator()
        {
            return new StringCalculator();
        }

        [TestMethod]
        public void Add_WithEmptyString_ResturnsZero()
        {
            var stringCalculator = BuildStringCalculator();

            var result = stringCalculator.Add(String.Empty);

            Assert.AreEqual(0, result);
        }

        [DataTestMethod]
        [DataRow("0", 0)]
        [DataRow("1", 1)]
        [DataRow("2", 2)]
        [DataRow("10", 10)]
        public void Add_WithOneNumber_ReturnsNumber(string number, int expected)
        {
            var stringCalculator = BuildStringCalculator();

            var result = stringCalculator.Add(number);

            Assert.AreEqual(expected, result);
        }

        [DataTestMethod]
        [DataRow("0,0", 0)]
        [DataRow("0,1", 1)]
        [DataRow("1,1", 2)]
        [DataRow("1,2", 3)]
        [DataRow("10,20", 30)]
        public void Add_WithTwoNumbers_ReturnsSum(string numbers, int expected)
        {
            var stringCalculator = BuildStringCalculator();

            var result = stringCalculator.Add(numbers);

            Assert.AreEqual(expected, result);
        }

        [DataTestMethod]
        [DataRow("0,1,2", 3)]
        [DataRow("1,2,3", 6)]
        [DataRow("11,22,33", 66)]
        [DataRow("1,20,300", 321)]
        public void Add_WithMultipleNumbers_ReturnSum(string numbers, int expected)
        {
            var stringCalculator = BuildStringCalculator();

            var result = stringCalculator.Add(numbers);

            Assert.AreEqual(expected, result);
        }
    }



    public class StringCalculator
    {
        public int Add(string numbers)
        {
            return
                numbers
                .Split(',')
                .Where(s => !String.IsNullOrEmpty(s))
                .Select(s => int.Parse(s))
                .Sum();
        }
    }
}
